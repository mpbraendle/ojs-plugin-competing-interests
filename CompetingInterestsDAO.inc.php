<?php

/**
 * @file CompetingInterestsDAO.inc.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @class CompetingInterestsDAO
 * @ingroup journal
 * @see Publication
 *
 * @brief Operations for retrieving and modifying Competing Interests objects.
 */

import ('classes.publication.PublicationDAO');

class CompetingInterestsDAO extends PublicationDAO {

	/**
	 * replace an existing Competing Interests record.
	 * @param $pubId integer
	 * @param $locale string
	 * @param $competingInterests array
	 */
	function replaceCompetingInterests($pubId, $locale, $competingInterests) {
        $updateArr = [
        			  'publication_id' => $pubId,
                      'locale' => $locale,
                      'setting_name' => 'competingInterests',
                      'setting_value'  => $competingInterests
                     ];

        $keys = ['publication_id', 'locale', 'setting_name'];

		$result = $this->replace('publication_settings',$updateArr, $keys);
		return $result;
	}

	/**
	 * Retrieve section_settings a publication is assigned to.
	 * @param $publicationId int Publication id
	 * @return Section_settings
	 */
	public function getByPublicationId($publicationId) {
		
		$result = $this->retrieve('SELECT s.* FROM section_settings s, publications p WHERE 
									p.section_id = s.section_id AND
									p.publication_id = ? AND 
									s.setting_name = ?',
									array((int) $publicationId, (string) 'competingInterestsNotRequired'));

		$row = $result->current();
		return $row;
	}

}
