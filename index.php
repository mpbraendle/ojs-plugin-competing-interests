<?php

/**
 * @defgroup plugins_generic_competingInterests Plugin
 */
 
/**
 * @file plugins/generic/competingInterests/index.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_generic_competingInterests
 *
 */

require_once('CompetingInterestsPlugin.inc.php');
return new CompetingInterestsPlugin();