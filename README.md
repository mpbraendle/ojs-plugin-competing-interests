# Competing Interests Plugin

This plugin is:
- adding new Competing Interests Statement text box field to submission form (Step 3)
- adding a checkbox to the Journal sections options, giving the ability to choose if the text box mentioned above has to be mandatory or optional.

# Installation

Compress the plugin in a `tar.gz` file and upload it through OJS plugin manager.

# What's new

- Getting the Competing Interests field on publication metadata tab.
- solved the restriction issue when sending balnk values.

## OJS compatibility

* OJS `3.3.*`: please install version version `1.10.*`+
* OJS `3.2.1-*`+: please install version `1.9.*`+
