<?php

/**
 * @file plugins/generic/competingInterests/CompetingInterestsPlugin.inc.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class CompetingInterestsPlugin
 * @ingroup plugins_generic_CompetingInterestsPlugin
 *
 * @brief CompetingInterests plugin class
 */

import('lib.pkp.classes.plugins.GenericPlugin');
use \PKP\components\forms\FieldRichTextarea;
class CompetingInterestsPlugin extends GenericPlugin {
    /**
     * @copydoc Plugin::register()
     */
    function register($category, $path, $mainContextId = null) {
        $success = parent::register($category, $path, $mainContextId);
        if ($success && $this->getEnabled($mainContextId)) {
            $this->import('CompetingInterestsDAO');
            $competingInterestsDao = new CompetingInterestsDAO();
            // Hooks for section options
            DAORegistry::registerDAO('CompetingInterestsDAO', $competingInterestsDao);
            HookRegistry::register('TemplateResource::getFilename', array($this, '_overridePluginTemplates'));
            HookRegistry::register('sectiondao::getAdditionalFieldNames', array($this, 'addSectionDAOFieldNames'));
            HookRegistry::register('sectionform::initdata', array($this, 'initDataSectionFormFields'));
            HookRegistry::register('sectionform::readuservars', array($this, 'readSectionFormFields'));
            HookRegistry::register('sectionform::execute', array($this, 'executeSectionFormFields'));

            // Hooks for Submission form step 3
            HookRegistry::register('Templates::Submission::SubmissionMetadataForm::AdditionalMetadata', array($this, 'metadataFieldEdit'));
            HookRegistry::register('submissionsubmitstep3form::initdata', array($this, 'metadataInitData'));
            HookRegistry::register('submissionsubmitstep3form::readuservars', array($this, 'metadataReadUserVars'));
            HookRegistry::register('submissionsubmitstep3form::execute', array($this, 'metadataExecute'));

            // Hooks to extend quick submit plugin form
            HookRegistry::register('quicksubmitform::display', array($this, 'metadataInitData'));
            HookRegistry::register('quicksubmitform::readuservars', array($this, 'metadataReadUserVars'));
            HookRegistry::register('quicksubmitform::execute', array($this, 'quickSumitExecute'));

            // Use a hook to extend the publication entity's schema
            HookRegistry::register('Schema::get::publication', array($this, 'addToSchema'));
            HookRegistry::register('Form::config::before', array($this, 'addToForm'));

            // Use a hook to extend the journal entity's schema
            HookRegistry::register('Schema::get::context', array($this, 'addToContextSchema'));
            HookRegistry::register('Form::config::before', array($this, 'addToAuthorGuidelinesForm'));
        }
        return $success;
    }

    /****************/
    /**** Plugin ****/
    /****************/
    function _overridePluginTemplates($hookName, $args) {
        $filePath  =& $args[0];
        $checkPath = 'plugins/importexport/quickSubmit/templates/index.tpl';
        $overriddenFilePath = 'templates/plugins/importexport/quicksubmit/templates/index.tpl';
        $fullPath = sprintf('%s/%s', $this->getPluginPath(), $overriddenFilePath);

        if (($filePath == $checkPath) && file_exists($fullPath)) {
            $filePath = $fullPath;
            $templateMgr = TemplateManager::getManager();
            $templateMgr->assign([
                'pageTitle' => __('plugins.importexport.quickSubmit.displayName'),
            ]);
        }


        return false;
    }

    /**
    * @copydoc Plugin::isSitePlugin()
    */
    function isSitePlugin() {
        // This is a site-wide plugin.
        return true;
    }

    /**
     * @copydoc Plugin::getDisplayName()
     * Get the plugin name 
     */
    function getDisplayName() {
        return __('plugins.generic.competingInterests.displayName');
    }

    /**
     * @copydoc Plugin::getDescription()
     * Get the description
     */
    function getDescription() {
        return __('plugins.generic.competingInterests.description');
    }

    /**
     * @copydoc Plugin::getInstallSitePluginSettingsFile()
     * get the plugin settings
     */
    function getInstallSitePluginSettingsFile() {
        return $this->getPluginPath() . '/settings.xml';
    }
    

    /**
     * Return the location of the plugin's CSS file
     *
     * @return string
     */
    function getStyleSheet() {
        return $this->getPluginPath() . '/css/competingInterests.css';
    }

    /******************************/
    /**** Publication Metadata ****/
    /******************************/

    /**
    * Extend the Publication entity's schema with an competingInterests property
    */
    public function addToSchema($hookName, $args) {

        $schema = $args[0];
        $schema->properties->competingInterests = (object)[
            'type' => 'string',
            'multilingual' => true,
            'validation' => ['nullable'],
        ]; 
    }

    /**
    * Extend the Publication form to add an competingInterests input field
    */
    public function addtoForm($hookName, $form) {

        // Only modify the metadata form
        if (!defined('FORM_METADATA') || $form->id !== FORM_METADATA) {
            return;
        }
        // get the Publication Id
        $actionArr = explode('/',$form->action);
        $publicationId = end($actionArr);

        // get the section settings
        $sectionDao = DAORegistry::getDAO('CompetingInterestsDAO');
        $section    = $sectionDao->getByPublicationId($publicationId);
        $competingInterestsNotRequired = $section->setting_value;

        // Add a field to the form
        $form->addField(new FieldRichTextarea('competingInterests', [
            'label' => __('manager.submissions.competingInterests.title'),
            'isMultilingual' => true,
            'isRequired' => !$competingInterestsNotRequired
        ]),array(FIELD_POSITION_BEFORE,'keywords'));
    }  

    /***********************************/
    /**** Workflow author guidlines ****/
    /***********************************/

    /**
    * Extend journal entity's schema with an competingInterests property
    */
    public function addToContextSchema($hookName, $args) {

        $schema = $args[0];
        $schema->properties->competingInterestsPolicy = (object)[
            'type' => 'string',
            'multilingual' => true,
            'validation' => ['nullable'],
            'defaultLocaleKey' => "plugins.generic.competingInterestsPolicy.default",
        ];
    }

    /**
    * Extend author guidelines form to add competing interests policy
    */
    public function addToAuthorGuidelinesForm($hookName, $form) {
                // Only modify author guidelines form
        if (!defined('FORM_AUTHOR_GUIDELINES') || $form->id !== FORM_AUTHOR_GUIDELINES) {
            return;
        }
        
        $request = Application::get()->getRequest();
        $context = $request->getContext();
        if (!$context) {
            return;
        }

        $dispatcher = $request->getDispatcher();
        $imageUploadUrl = $dispatcher->url($request, ROUTE_API, $context->getPath(), '_uploadPublicFile');
        $locale = AppLocale::getLocale();
        $competingInterestsPolicy = $context->getData('competingInterestsPolicy');
        if (!$competingInterestsPolicy) {
            $value = [$locale => __('plugins.generic.competingInterests.default')];
            $context->updateSetting('competingInterestsPolicy', $value);
            $competingInterestsPolicy = $value;
        }

        // Add competing interests policy field
        $form->addField(new FieldRichTextarea('competingInterestsPolicy', [
                        'label' => __('manager.setup.competingInterestsPolicy'),
                        'isMultilingual' => true,
                        'groupId' => 'keyInfo',
                        'toolbar' => 'bold italic superscript subscript | link | blockquote bullist numlist | image | code',
                        'plugins' => 'paste,link,lists,image,code',
                        'uploadUrl' => $imageUploadUrl,
                        'value' => $competingInterestsPolicy,
        ]));
    }


    /************************/
    /**** Section option ****/
    /************************/

    /**
     * Add section settings to SectionDAO
     *
     * @param $hookName string
     * @param $args array
     *
     */
    public function addSectionDAOFieldNames($hookName, $args) {
        //add the new field name to the form
        $fields   =& $args[1];
        $fields[] = 'competingInterestsNotRequired';
    }

    /**
     * Initialize data when form is first loaded
     *
     * @param $hookName string
     * @param $args array [
     *      @option SectionForm
     * ]
     */
    public function initDataSectionFormFields($hookName, $args) {
        // initiating the new section option value
        // get the current form values
        $form = $args[0];

        // get the context id
        $request   = PKPApplication::get()->getRequest();
        $context   = $request->getContext();
        $contextId = $context->getId();
        $sectionId = $form->getSectionId();

        // get the current section option value from db
        $sectionDao = DAORegistry::getDAO('SectionDAO');
        $section    = $sectionDao->getById($sectionId, $contextId);
        if($section) {
            $competingInterestsNotRequired = $section->getData('competingInterestsNotRequired');
            // add the current section option value to the form
            if ($section) $form->setData('competingInterestsNotRequired', $competingInterestsNotRequired);
        }
        $templateMgr = TemplateManager::getManager($request);
        $templateMgr->assign('competingInterestsPluginEnabled',true);
    }

    /**
     * Read user input from additional fields in the section editing form
     *
     * @param $hookName string `sectionform::readUserVars`
     * @param $args array [
     *      @option SectionForm   
     * ]
     */
    public function readSectionFormFields($hookName, $args) {
        // add the new section option value to the current form
        $sectionForm =& $args[0];
        $request = PKPApplication::get()->getRequest();
        $competingInterestsNotRequired = $request->getUserVar('competingInterestsNotRequired');
        $sectionForm->setData('competingInterestsNotRequired', $competingInterestsNotRequired);
    }

    /**
     * Save additional fields in the section editing form
     *
     * @param $hookName string
     * @param $args array
     *
     */
    public function executeSectionFormFields($hookName, $args) {
        // submitting the form
        // get the current form values
        $sectionForm = $args[0];
        
        // get the new section option value
        $competingInterestsNotRequired = $sectionForm->getData('competingInterestsNotRequired') ? $sectionForm->getData('competingInterestsNotRequired') :0;

        // get the section option value from db
        $sectionDao = DAORegistry::getDAO('SectionDAO');
        $section    = $sectionDao->getById($sectionForm->getSectionId()); 
        // add the new value to section option value          
        $section->setData('competingInterestsNotRequired', $competingInterestsNotRequired);
        // update the section option value in db
        $sectionDao->updateObject($section);
    }

    /*******************************/
    /***** Submission Metadata *****/
    /*******************************/

    /**
     * Insert Competing Interests field into meta data submission step 3 and metadata edit form
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataFieldEdit($hookName, $params) {
        // get the template info
        $smarty =& $params[1];
        $output =& $params[2];
        // add the new Competing Interests field block to the template form
        $output .= $smarty->fetch($this->getTemplateResource('competingInterests.tpl'));
        return false;
    }

    /**
     * Init Competing Interests value
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataInitData($hookName, $params) {
        // initiating Competing Interests value
        // get the current form values
        $form =& $params[0];
        $request = PKPApplication::get()->getRequest();
        $context = $request->getContext();
        // get the template
        $templateMgr = TemplateManager::getManager($request);

        if (get_class($form) == 'SubmissionSubmitStep3Form') {
            // get the publiction id and locale
            $publication = $form->submission->getCurrentPublication();
            $pubId = $publication->getData('id');
            $locale  = $publication->getData('locale');
            $sectionId = $publication->getData('sectionId');
            $competingInterests = $publication->getData('competingInterests');
            
            if (!$competingInterests) {
                $competingInterests = array($locale => null);
            }

            // add the value to the form
            $publication->setData('competingInterests', $competingInterests);
           
            // check if the field is required/optional
            $sectionDao = DAORegistry::getDAO('SectionDAO');
            $section    = $sectionDao->getById($sectionId, $context->getId());       
            $competingInterestsNotRequired = $section->getData('competingInterestsNotRequired');
            $competingIrequired = !$competingInterestsNotRequired;

            // get competing interests policy
            $context = Application::getRequest()->getContext();
            $competingInterestsPolicy = $context->getLocalizedData('competingInterestsPolicy');
            // add the current data to the template
            $templateVars = array('competingInterests'=>$competingInterests,
                                  'competingIrequired'=>$competingIrequired,
                                  'competingInterestsPolicy'=>$competingInterestsPolicy ? $competingInterestsPolicy : __('plugins.generic.competingInterests.default'));

            $templateMgr->assign($templateVars);

        }  elseif ($hookName == 'quicksubmitform::display') {

            $sectionDao = DAORegistry::getDAO('SectionDAO');
            $sectionId = $form->getData('sectionId');
            $section = $sectionDao->getById($sectionId, $context->getId());
            $competingInterestsNotRequired = false;
            if ($section) {
                $competingInterestsNotRequired = $section->getData('competingInterestsNotRequired');
                $competingIrequired = !$competingInterestsNotRequired;
                if ($competingIrequired) {
                    $form->addCheck(new FormValidatorLocale($form,'competingInterests','required','manager.submissions.competingInterests.error'));
                }
            }
            // get competing interests policy
            $context = Application::getRequest()->getContext();
            $competingInterestsPolicy = $context->getLocalizedData('competingInterestsPolicy');
            // add the current data to the template
            $templateVars = array('competingInterests'=>$competingInterests,
                                  'competingIrequired'=>$competingIrequired,
                                  'competingInterestsPolicy'=>$competingInterestsPolicy ? $competingInterestsPolicy : __('plugins.generic.competingInterests.default'));

            $templateMgr->assign($templateVars);
        }

        return false;
    }

    /**
     * Get Competing Interests field value from the form
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataReadUserVars($hookName, $params) {
        // get the template
        $request = PKPApplication::get()->getRequest();
        $templateMgr = TemplateManager::getManager($request);
        $context = $request->getContext();
        // get the form values
        $form = $params[0];
        // add the new field values to the user variables
        $userVars =& $params[1];
        $userVars[] = 'competingInterests';

        if ($hookName == 'submissionsubmitstep3form::readuservars') {
            // check if the new value is matching the field restrictions
            $sectionId  = $form->submission->getCurrentPublication()->getData('sectionId');
        } elseif ($hookName == 'quicksubmitform::readuservars') {
            $requestVars = $request->getUserVars();
            $sectionId = $requestVars['sectionId'];
        }

        $sectionDao = DAORegistry::getDAO('SectionDAO');
        $section    = $sectionDao->getById($sectionId, $context->getId());
        $competingInterestsNotRequired = false;
        if ($section) {
            $competingInterestsNotRequired = $section->getData('competingInterestsNotRequired');
        }
        $templateMgr->assign('competingIrequired',!$competingInterestsNotRequired);
        $competingInterestsrequired = (!$competingInterestsNotRequired)?'required':'optional';
        $form->addCheck(new FormValidatorLocale($form,'competingInterests',$competingInterestsrequired,'manager.submissions.competingInterests.error'));

        return false;
    }

    /**
     * Set competingInterests value
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataExecute($hookName, $params) {
        // submitting the form with the new Competing Interests value
        // get the form values
        $form =& $params[0];
        $publication = $form->submission->getCurrentPublication();
        
        $pubId = $publication->getData('id');
        $sectionId = $publication->getData('sectionId');

        // get the current Competing Interests value
        $competingInterests = $form->getData('competingInterests');
        if (!$competingInterests) {
            $competingInterests = array($locale => '');
        } 

        // update the data
        $competingInterestsDao = DAORegistry::getDAO('CompetingInterestsDAO');
        foreach ($competingInterests as $locale => $value) {
            $competingInterestsDao->replaceCompetingInterests($pubId, $locale, $value);
        }
        

        return false;
    }

    /*******************************/
    /***** quickSubmit Plugin ******/
    /*******************************/

    /**
     * Set competingInterests value via quickSubmit plugin
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function quickSumitExecute($hookName, $params) {
        // submitting the form with the new Competing Interests value
        // via quickSubmit plugin
        $request = PKPApplication::get()->getRequest();
        $form = $params[1];
        $pubId = $form->getData('currentPublicationId');
        $requestVars = $request->getUserVars();
        $competingInterests = $requestVars['competingInterests'];
        if (!$competingInterests) {
            $competingInterests = array($locale => '');
        } 

        // update the data
        $competingInterestsDao = DAORegistry::getDAO('CompetingInterestsDAO');
        foreach ($competingInterests as $locale => $value) {
            $competingInterestsDao->replaceCompetingInterests($pubId, $locale, $value);
        }

        return false;
    }
}

